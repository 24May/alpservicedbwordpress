<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 18.06.14
 * Time: 15:41
 */
global $wpdb;
global $current_user;
$table_db_staff = $wpdb->prefix.db_staff;
$table_db_object = $wpdb->prefix.db_object;
$table_db_link = $wpdb->prefix.db_link;
$table_db_spec = $wpdb->prefix.db_spec;

$id = trim($_GET['edit']);
if($_POST['update_staff']) {
    if ($_FILES['photo'] and $_FILES['photo']['size'] != 0) {
        if($_FILES['photo']['size'] > 524288 ) {
            die ("<h2 style='color: red;'>Ваш файл большого размера. Допускается размер до 512 KB</h2>
        <h3>Нажмите в браузере кнопку 'НАЗАД'</h3>");
        }
        include "upload_photo.php";
    }
    function RemoveSpace($data) {
        return trim($data);
    }


//    echo 'Имя пользователя: ' . $current_user->user_login . "\n";
    //Добавление при наличии и отсутствия фото
    if($current_user->user_login != "ok") {
        include_once "upload-for-ok.php";
    } else {
        if($_FILES['photo']['size'] != 0) {
            $w = $wpdb->update($table_db_staff,
                array(
                    'fio' => RemoveSpace($_POST['fio']),
                    'sex' => RemoveSpace($_POST['sex']),
                    'born' => RemoveSpace($_POST['born']),
                    'position' => RemoveSpace($_POST['position']),
                    'phone' => RemoveSpace($_POST['phone']),
                    'address' => RemoveSpace($_POST['address']),
                    'experience' => RemoveSpace($_POST['experience']),
                    'data_learning' => RemoveSpace($_POST['data_learning']),
                    'photo' => $targetwww,
                    'mark' => RemoveSpace($_POST['mark'])
                ),
                array(
                    'id' => $id
                )
            );
        } else {
            $w = $wpdb->update($table_db_staff,
                array(
                    'fio' => RemoveSpace($_POST['fio']),
                    'sex' => RemoveSpace($_POST['sex']),
                    'born' => RemoveSpace($_POST['born']),
                    'position' => RemoveSpace($_POST['position']),
                    'phone' => RemoveSpace($_POST['phone']),
                    'address' => RemoveSpace($_POST['address']),
                    'experience' => RemoveSpace($_POST['experience']),
                    'data_learning' => RemoveSpace($_POST['data_learning']),
                    'mark' => RemoveSpace($_POST['mark'])
                ),
                array(
                    'id' => $id
                )
            );
        }
    }

    //$wpdb->show_errors();
    //$wpdb->print_error();

    $wpdb->delete( $table_db_link, array( 'id_user' => $_GET['edit']));

    $count_object = count($_POST['object']);
    $count_spec = count($_POST['spec']);
    $min = array();
    $max = array();
    $new = array();
    if($count_object >= $count_spec) {
        $max = $_POST['object'];
        $min = $_POST['spec'];
        $q = true;
    } else {
        $min = $_POST['object'];
        $max = $_POST['spec'];
        $q = false;
    }
    for ($i=0; $i < count($max); $i++) {
        if($min[$i] == null) {
            $min[$i] = 1;
        }
        $new[] = '('.$_GET['edit'].','.$max[$i].','.$min[$i].')';

    }
    if($q) {
        $insert = "INSERT INTO ".$table_db_link." (id_user, id_object, id_spec) VALUES ".implode(',', $new)."";
} else {
        $insert = "INSERT INTO ".$table_db_link." (id_user, id_spec, id_object) VALUES ".implode(',', $new)."";
    }
    $ww = $wpdb->query($insert);
            echo "<h1 style='color: #003bb3;'>Информация о сотруднике обновлена</h1>";
        ?>
        <p style="font-size: 14px; font-weight: bolder;"><a href="<?=$_SERVER[PHP_SELF]; ?>?page=theme-op-settings">НАЗАД К СПИСКУ</a></p>
        <?php
        exit;

}



$select ="SELECT  ".$table_db_staff.".*, ".$table_db_object.".*, ".$table_db_spec.".* FROM ".$table_db_staff." INNER JOIN ".$table_db_link." ON ".$table_db_staff.".id = ".$table_db_link.".id_user INNER JOIN ".$table_db_object." ON ".$table_db_object.".id = ".$table_db_link.".id_object INNER JOIN ".$table_db_spec." ON ".$table_db_spec.".id = ".$table_db_link.".id_spec WHERE ".$table_db_staff.".id = ".$id."";

$select = $wpdb->get_results($select, ARRAY_A);

$select_o = "SELECT id, object FROM ".$table_db_object."";
$data_o = $wpdb->get_results($select_o, ARRAY_A);

$select_s = "SELECT id, add_spec FROM ".$table_db_spec."";
$data_s = $wpdb->get_results($select_s, ARRAY_A);

echo "<h1>Страница редактирования</h1>";

?>
<form  action="" method="post" enctype="multipart/form-data" >
    <table>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Фамилия Имя Отчество</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['fio']; ?>" name="fio"  />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Фото<br />
            <?php if (!$select[0]['photo']) {
                echo "<img src=".WP_CONTENT_URL."/uploads/photo/noimage.jpg />";
            } else {
                echo "<img src=".$select[0]['photo']." />";
            }
            ?>
            </td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="file"  name="photo" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Пол</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <select name="sex">
                    <option <?php if($select[0]['sex'] == "M") echo "selected"; ?> value="M">мужской</option>
                    <option <?php if($select[0]['sex'] == "F") echo "selected"; ?> value="F">женский</option>
                </select> </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Дата рождения</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input class="datepicker"  type="text" size="50px" value="<?=$select[0]['born'] ;?>" name="born" />
            </td>
        </tr>
        <?php if($current_user->user_login != "ok")  : ?>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Тип образования</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['id_edu']; ?>" name="id_edu" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Специальность по диплому</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['spec']; ?>" name="specialist" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Учебное заведение</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=trim(str_replace('"', ' ', $select[0]['education'])); ?>" name="education" />
            </td>
        </tr>
        <?php endif; ?>

        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Должность на предприятии</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['position']; ?>" name="position" />
            </td>
        </tr>
        <?php if($current_user->user_login != "ok")  : ?>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">ИНН</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['inn']; ?>" name="inn" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Документ удост. личность</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['doc']; ?>" name="doc" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Серия</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['series']; ?>" name="series" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Номер</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['number']; ?>" name="number" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Дата выдачи</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input class="datepicker"  type="text" value="<?=$select[0]['date_doc']; ?>" size="50px" name="date_doc" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Кем выдан</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['who_doc']; ?>" name="who_doc" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Начало работы</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input class="datepicker" type="text" size="50px" value="<?=$select[0]['start_job']; ?>" name="start_job" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Конец работы</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input class="datepicker" type="text" size="50px" value="<?=$select[0]['finish_job']; ?>" name="finish_job" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Дети</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['child']; ?>" name="child" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Год рождения ребёнка</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['date_child']; ?>" name="date_child" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Номер приказа и дата о приёме на работу</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['order_start_job']; ?>" name="order_start_job" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Номер приказа и дата о увольнении с работы</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['order_finish_job']; ?>" name="order_finish_job" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Инвалидность</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <select name="invalid">
                    <option <?php if($select[0]['invalid'] == "N") echo "selected"; ?> value="N">нет</option>
                    <option <?php if($select[0]['invalid'] == "Y") echo "selected"; ?> value="Y">да</option>
                </select>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Семейное состояние</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['id_family']; ?>" name="id_family" />
            </td>
        </tr>
        <?php endif; ?>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Телефон</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['phone']; ?>" name="phone" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Адрес</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['address']; ?>" name="address" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Опыт работы</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input type="text" size="50px" value="<?=$select[0]['experience']; ?>" name="experience" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Дата прохождения обучению высотным работам</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input class="datepicker" type="text" value="<?=$select[0]['data_learning']; ?>" size="50px" name="data_learning" />
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Объекты на которых работал</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <select name="object[]" style="width: 470px;" multiple size="3" multiple="multiple">
                    <?php
                    foreach($data_o as  $value) {
                        ?>
                        <option <?php
                        for($i = 0; $i < count($select); $i++ ) {
                            if($select[$i]['object'] == $value['object'] ) {
                                echo "selected";
                            }
                        }
                        ?> value="<?=$value['id']; ?>"><?=$value['object']; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Смежные профессии</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <select name="spec[]" style="width: 470px;" multiple size="3" multiple="multiple">
                    <?php
                    foreach($data_s as  $value) {
                        ?>
                        <option <?php
                        for($i = 0; $i < count($select); $i++ ) {
                            if($select[$i]['add_spec'] == $value['add_spec'] ) {
                                echo "selected";
                            }
                        }
                        ?> value="<?=$value['id']; ?>"><?=$value['add_spec']; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Оценка прораба, мастера, начальника участка</td>
            <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
                <input class="mark" type="text" value="<?=$select[0]['mark']; ?>" size="50px" name="mark" />
            </td>
        </tr>
        <tr>
            <td></td><td style="text-align: right;"><input type="submit" class="button button-primary" name="update_staff" value="Обновить информацию о сотруднике" /></td>
        </tr>
    </table>
    </form>
