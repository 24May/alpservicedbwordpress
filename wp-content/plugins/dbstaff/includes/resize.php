<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 21.06.14
 * Time: 21:16
 */
function resizeImage($filename) {
    $final_width_of_image = 166;
    $im = imagecreatefromjpeg($filename);
    $ox = imagesx($im);
    $oy = imagesy($im);
    $nx = $final_width_of_image;
    $ny = floor($oy * ($final_width_of_image / $ox));

    $nm = imagecreatetruecolor($nx, $ny);

    imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
    imagejpeg($nm, $filename."-thumbs.jpg");
}
