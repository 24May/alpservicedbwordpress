<?php
global $current_user;
if($_POST['adding_staff'] and trim($_POST['fio'])) {
    if ($_FILES['photo'] and $_FILES['photo']['size'] != 0) {
        if($_FILES['photo']['size'] > 524288 ) {
            die ("<h2 style='color: red;'>Ваш файл большого размера. Допускается размер до 512 KB</h2>
        <h3>Нажмите в браузере кнопку 'НАЗАД'</h3>");
        }
        include "upload_photo.php";

    }


    function CheckEmpty($data) {
        if(!is_array($data)) {


        $data = trim($data);
        if($data == "") {
            $data = "-";
        }
        }
        return $data;
    }
    if($current_user->user_login != "ok") {
        global $wpdb;
        $table_db = $wpdb->prefix.db_staff;
        $insert = "INSERT INTO `".$table_db."` (
                            fio,
                            sex,
                            born,
                            spec,
                            education,
                            position,
                            inn,
                            doc,
                            date_doc,
                            who_doc,
                            number,
                            series,
                            start_job,
                            finish_job,
                            child,
                            date_child,
                            order_start_job,
                            order_finish_job,
                            invalid,
                            id_edu,
                            id_family,
                            phone,
                            address,
                            experience,
                            data_learning,
                            photo,
                            mark
                            )
                           VALUES (
                              '".CheckEmpty($_POST['fio'])."',
                              '".CheckEmpty($_POST['sex'])."',
                              '".CheckEmpty($_POST['born'])."',
                              '".CheckEmpty($_POST['specialist'])."',
                              '".CheckEmpty($_POST['education'])."',
                              '".CheckEmpty($_POST['position'])."',
                              '".CheckEmpty($_POST['inn'])."',
                              '".CheckEmpty($_POST['doc'])."',
                              '".CheckEmpty($_POST['date_doc'])."',
                              '".CheckEmpty($_POST['who_doc'])."',
                              '".CheckEmpty($_POST['number'])."',
                              '".CheckEmpty($_POST['series'])."',
                              '".CheckEmpty($_POST['start_job'])."',
                              '".CheckEmpty($_POST['finish_job'])."',
                              '".CheckEmpty($_POST['child'])."',
                              '".CheckEmpty($_POST['date_child'])."',
                              '".CheckEmpty($_POST['order_start_job'])."',
                              '".CheckEmpty($_POST['order_finish_job'])."',
                              '".CheckEmpty($_POST['invalid'])."',
                              '".CheckEmpty($_POST['id_edu'])."',
                              '".CheckEmpty($_POST['id_family'])."',
                              '".CheckEmpty($_POST['phone'])."',
                              '".CheckEmpty($_POST['address'])."',
                              '".CheckEmpty($_POST['experience'])."',
                              '".CheckEmpty($_POST['data_learning'])."',
                              '".$targetwww."',
                              '".CheckEmpty($_POST['mark'])."'
                           )

                ";
    } else {
        global $wpdb;
        $table_db = $wpdb->prefix.db_staff;
        $insert = "INSERT INTO `".$table_db."` (
                            fio,
                            sex,
                            born,
                            position,
                            phone,
                            address,
                            experience,
                            data_learning,
                            photo,
                            mark
                            )
                           VALUES (
                              '".CheckEmpty($_POST['fio'])."',
                              '".CheckEmpty($_POST['sex'])."',
                              '".CheckEmpty($_POST['born'])."',
                              '".CheckEmpty($_POST['position'])."',
                              '".CheckEmpty($_POST['phone'])."',
                              '".CheckEmpty($_POST['address'])."',
                              '".CheckEmpty($_POST['experience'])."',
                              '".CheckEmpty($_POST['data_learning'])."',
                              '".$targetwww."',
                              '".CheckEmpty($_POST['mark'])."'
                           )

                ";
    }

    $w = $wpdb->query($insert);
    $added_id =  $wpdb->insert_id;
    //$wpdb->show_errors();
    //$wpdb->print_error();

    $count_object = count($_POST['object']);
    $count_spec = count($_POST['spec']);
    $min = array();
    $max = array();
    $new = array();
    if($count_object >= $count_spec) {
        $max = $_POST['object'];
        $min = $_POST['spec'];
        $q = true;
    } else {
        $min = $_POST['object'];
        $max = $_POST['spec'];
        $q = false;
    }
    for ($i=0; $i < count($max); $i++) {
        if($min[$i] == null) {
            $min[$i] = 1;
        }
        $new[] = '('.$added_id.','.$max[$i].','.$min[$i].')';
    }


    $table_db = $wpdb->prefix.db_link;

    if($q) {
        $insert = "INSERT INTO ".$table_db." (id_user, id_object, id_spec) VALUES ".implode(',', $new)."";
    } else {
        $insert = "INSERT INTO ".$table_db." (id_user, id_spec, id_object) VALUES ".implode(',', $new)."";
    }

    $ww = $wpdb->query($insert);
    //$wpdb->show_errors();
    //$wpdb->print_error();



    if($w and $ww) {
        echo "<h1>Сотрудник добавлен в базу данных</h1>";
        ?>
        <p style="font-size: 14px; font-weight: bolder;"><a href="<?=$_SERVER[PHP_SELF]; ?>?page=theme-op-settings">НАЗАД К СПИСКУ</a></p>
        <p style="font-size: 14px; font-weight: bolder;"><a href="<?=$_SERVER[PHP_SELF]; ?>?page=theme-op-settings&add=new">ДОБАВИТЬ ЕЩЁ ОДНОГО СОТРУДНИКА</a></p>
        <?php

        exit;
    }
}


global $wpdb;
$table_db = $wpdb->prefix.db_object;
$table_db_learning = $wpdb->prefix.db_learning;
$table_db_spec = $wpdb->prefix.db_spec;

$select_object = "SELECT id, object FROM  `".$table_db."`";
$select_learning = "SELECT id, learning FROM  `".$table_db_learning."`";
$select_spec = "SELECT id, add_spec FROM  `".$table_db_spec."`";

$data = $wpdb->get_results($select_object, ARRAY_A);
$data_learning = $wpdb->get_results($select_learning, ARRAY_A);
$data_spec = $wpdb->get_results($select_spec, ARRAY_A);

echo "<h1>Страница ввода нового сотрудника</h1>";
global $current_user;
//echo 'Имя пользователя: ' . $current_user->user_login . "\n";
?>
<div style="overflow: scroll; overflow-x: hidden; height: 840px;">
<form enctype="multipart/form-data" action="" method="post"  >
<table>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Фамилия Имя Отчество</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="fio" />
            <!-- <input type="hidden" name="MAX_FILE_SIZE" value="524288" /> -->
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Фото</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="file"  name="photo" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Пол</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <select name="sex">
                <option value="M">мужской</option>
                <option value="F">женский</option>
            </select> </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Дата рождения</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input class="datepicker" type="text" size="50px" name="born" />
        </td>
    </tr>
    <?php if($current_user->user_login != "ok")  : ?>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Тип образования</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="id_edu" />
        </td>
    </tr>

    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Специальность по диплому</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="specialist" />
        </td>
    </tr>

    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Учебное заведение</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="education" />
        </td>
    </tr>
    <?php endif; ?>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Должность на предприятии</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="position" />
        </td>
    </tr>
    <?php if($current_user->user_login != "ok")  : ?>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">ИНН</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="inn" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Документ удост. личность</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="doc" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Серия</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="series" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Номер</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="number" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Дата выдачи</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input class="datepicker"  type="text" size="50px" name="date_doc" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Кем выдан</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="who_doc" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Начало работы</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input class="datepicker" type="text" size="50px" name="start_job" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Конец работы</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input class="datepicker" type="text" size="50px" name="finish_job" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Дети</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="child" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Год рождения ребёнка</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="date_child" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Номер приказа и дата о приёме на работу</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="order_start_job" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Номер приказа и дата о увольнении с работы</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="order_finish_job" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Инвалидность</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <select name="invalid">
                <option value="N">нет</option>
                <option value="Y">да</option>
            </select>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Семейное состояние</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="id_family" />
        </td>
    </tr>
    <?php endif; ?>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Телефон</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="phone" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Адрес</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="address" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Опыт работы</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input type="text" size="50px" name="experience" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Дата прохождения обучению высотным работам</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input class="datepicker" type="text" size="50px" name="data_learning" />
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Объекты на которых работал</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <select name="object[]" style="width: 470px;" multiple size="3" multiple="multiple">
                <?php
                foreach($data as  $value) {
                ?>
                   <option <?php if($value['id'] == 1)  echo "selected";  ?> value="<?=$value['id']; ?>"><?=$value['object']; ?></option>
                <?php
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Смежные профессии</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <select name="spec[]" style="width: 470px;" multiple size="3" multiple="multiple">
                <?php
                foreach($data_spec as  $value) {
                    ?>
                    <option <?php if($value['id'] == 1)  echo "selected";  ?> value="<?=$value['id']; ?>"><?=$value['add_spec']; ?></option>
                <?php
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid black; padding: 5px; color: #008000; font-weight: bold; font-size: 14px;">Оценка прораба, мастера, начальника участка</td>
        <td style="border-bottom: 1px solid black; padding: 5px;  font-weight: bold; font-size: 14px;">
            <input  type="text" size="50px" name="mark" />
        </td>
    </tr>
    <tr>
        <td></td><td style="text-align: right;"><input type="submit" class="button button-primary" name="adding_staff" value="Записать" /></td>
    </tr>
</table>

</form>
</div>