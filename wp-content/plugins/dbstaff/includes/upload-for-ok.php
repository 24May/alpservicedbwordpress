<?php
if($_FILES['photo']['size'] != 0) {
    $w = $wpdb->update($table_db_staff,
        array(
            'fio' => RemoveSpace($_POST['fio']),
            'sex' => RemoveSpace($_POST['sex']),
            'born' => RemoveSpace($_POST['born']),
            'spec' => RemoveSpace($_POST['specialist']),
            'education' => RemoveSpace($_POST['education']),
            'position' => RemoveSpace($_POST['position']),
            'inn' => RemoveSpace($_POST['inn']),
            'doc' => RemoveSpace($_POST['doc']),
            'date_doc' => RemoveSpace($_POST['date_doc']),
            'who_doc' => RemoveSpace($_POST['who_doc']),
            'number' => RemoveSpace($_POST['number']),
            'series' => RemoveSpace($_POST['series']),
            'start_job' => RemoveSpace($_POST['start_job']),
            'finish_job' => RemoveSpace($_POST['finish_job']),
            'child' => RemoveSpace($_POST['child']),
            'date_child' => RemoveSpace($_POST['date_child']),
            'order_start_job' => RemoveSpace($_POST['order_start_job']),
            'order_finish_job' => RemoveSpace($_POST['order_finish_job']),
            'invalid' => RemoveSpace($_POST['invalid']),
            'id_edu' => RemoveSpace($_POST['id_edu']),
            'id_family' => RemoveSpace($_POST['id_family']),
            'phone' => RemoveSpace($_POST['phone']),
            'address' => RemoveSpace($_POST['address']),
            'experience' => RemoveSpace($_POST['experience']),
            'data_learning' => RemoveSpace($_POST['data_learning']),
            'photo' => $targetwww,
            'mark' => RemoveSpace($_POST['mark'])
        ),
        array(
            'id' => $id
        )
    );
} else {
    $w = $wpdb->update($table_db_staff,
        array(
            'fio' => RemoveSpace($_POST['fio']),
            'sex' => RemoveSpace($_POST['sex']),
            'born' => RemoveSpace($_POST['born']),
            'spec' => RemoveSpace($_POST['specialist']),
            'education' => RemoveSpace($_POST['education']),
            'position' => RemoveSpace($_POST['position']),
            'inn' => RemoveSpace($_POST['inn']),
            'doc' => RemoveSpace($_POST['doc']),
            'date_doc' => RemoveSpace($_POST['date_doc']),
            'who_doc' => RemoveSpace($_POST['who_doc']),
            'number' => RemoveSpace($_POST['number']),
            'series' => RemoveSpace($_POST['series']),
            'start_job' => RemoveSpace($_POST['start_job']),
            'finish_job' => RemoveSpace($_POST['finish_job']),
            'child' => RemoveSpace($_POST['child']),
            'date_child' => RemoveSpace($_POST['date_child']),
            'order_start_job' => RemoveSpace($_POST['order_start_job']),
            'order_finish_job' => RemoveSpace($_POST['order_finish_job']),
            'invalid' => RemoveSpace($_POST['invalid']),
            'id_edu' => RemoveSpace($_POST['id_edu']),
            'id_family' => RemoveSpace($_POST['id_family']),
            'phone' => RemoveSpace($_POST['phone']),
            'address' => RemoveSpace($_POST['address']),
            'experience' => RemoveSpace($_POST['experience']),
            'data_learning' => RemoveSpace($_POST['data_learning']),
            'mark' => RemoveSpace($_POST['mark'])
        ),
        array(
            'id' => $id
        )
    );
}