<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 21.06.14
 * Time: 18:08
 * 166px × 176px
 */
$upload_dir = $_SERVER['DOCUMENT_ROOT']."/wp-content/uploads/photo";
if (empty($_FILES['photo']['name'])) {
    $upload_file = $upload_dir."/noimage.jpg";
} else {
    if(preg_match('/[.](JPG)|(jpg)|(jpeg)|(JPEG)$/',$_FILES['photo']['name'])) {

        $upload_file = uniqid().".jpg";
        $target = $upload_dir."/".$upload_file;
        if(move_uploaded_file($_FILES['photo']['tmp_name'], $target)) {
            include "resize.php";
            resizeImage($target);
            $path_to_thumbs_directory = WP_CONTENT_URL."/uploads/photo/";
            $targetwww = WP_CONTENT_URL."/uploads/photo/".$upload_file."-thumbs.jpg";
        }

    } else {
        die("<h2 style='color: red;'>Используйте файл с расширением JPG или JPEG</h2>
        <h3>Нажмите в браузере кнопку 'НАЗАД'</h3>
        ");
    }

}
