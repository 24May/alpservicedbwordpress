<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 26.06.14
 * Time: 16:21
 */
function GetInfo() {
    //echo "<h4>Дни рождения в текущем месяце</h4>";
    $connect_db = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
    mysql_query("SET NAMES 'utf8'");
    mysql_select_db("wpdb", $connect_db);

    $data_born = mysql_query("SELECT fio, born FROM wp_db_staff WHERE MONTH(born) = ".date('m')." ORDER BY DAY(born) ASC", $connect_db);

    $next_month = 1 + (int)(date('m'));
    $next_month_data_born = mysql_query("SELECT fio, born FROM wp_db_staff WHERE MONTH(born) = $next_month ORDER BY DAY(born) ASC", $connect_db);
    $now_date = date('d', time());



    ?>
    <table class="table table-hover">
        <caption><h4>Дни рождения в текущем месяце</h4></caption>
       <tr class="success"><td>Сотрудник</td><td>Дата Рождения</td></tr>
<?php
    while($line = mysql_fetch_array($data_born)) {
        if($now_date != date('d', strtotime($line['born']) )) {
            printf("<tr><td>%s</td><td>%s</td></tr>",$line['fio'],date('d.m.Y', strtotime($line['born']) )  );
        } else {
            printf("<tr class='error'><td><strong>%s</strong></td><td><strong>%s</strong></td></tr>",$line['fio'],date('d.m.Y', strtotime($line['born']) )  );
        }

    }
    ?>
    </table>

    <table class="table table-hover">
        <caption><h4>Дни рождения в следующем месяце</h4></caption>
        <tr class="success"><td>Сотрудник</td><td>Дата Рождения</td></tr>
        <?php
        while($line = mysql_fetch_array($next_month_data_born)) {

                printf("<tr><td>%s</td><td>%s</td></tr>",$line['fio'],date('d.m.Y', strtotime($line['born']) )  );

        }
        ?>
    </table>
    <?php


}
add_shortcode('info', 'GetInfo');