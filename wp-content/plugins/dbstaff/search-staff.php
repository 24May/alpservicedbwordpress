<?php
function myshortcode($atts, $content = "") {
    ?>
    <h5>Поиск сотрудника</h5>
    <form class="form-search" action="" method="post">
        <!-- <input type="text" name="search_name" class="input-medium search-query" />
        <button type="submit" class="btn">Найти</button> -->
        <div class="input-append">
            <input type="text" name="search_name" class="span2 search-query" placeholder="Введите первые буквы фамилии">
            <button type="submit" class="btn">Найти</button>
        </div>
    </form>

    <?php

    if($_POST['search_name'] and $_POST['search_name'] != "") {
        $search = $_POST['search_name'];
        global $wpdb;
        $table_db = $wpdb->prefix.db_staff;
        $table_db_staff = $wpdb->prefix.db_staff;
        $table_db_object = $wpdb->prefix.db_object;
        $table_db_link = $wpdb->prefix.db_link;
        $table_db_spec = $wpdb->prefix.db_spec;

        //$select = "SELECT fio, photo, born, position, experience, data_learning, mark, address, phone   from `".$table_db."` WHERE fio LIKE '".$search."%' ";

        $select ="SELECT ".$table_db_staff.".*, ".$table_db_object.".*, ".$table_db_spec.".*
                    FROM ".$table_db_staff."
                    INNER JOIN ".$table_db_link." ON ".$table_db_staff.".id = ".$table_db_link.".id_user
                    INNER JOIN ".$table_db_object." ON ".$table_db_object.".id = ".$table_db_link.".id_object
                    INNER JOIN ".$table_db_spec." ON ".$table_db_spec.".id = ".$table_db_link.".id_spec
                    WHERE ".$table_db_staff.".fio LIKE '".$search."%'
                    GROUP BY  ".$table_db_staff.".fio
                    HAVING COUNT(*) > 1 ";

        $selec = "SELECT ".$table_db_staff.".* FROM  ".$table_db_staff." WHERE ".$table_db_staff.".fio LIKE '".$search."%' and ".$table_db_staff.".id IN (
                    SELECT ".$table_db_link.".id_user FROM ".$table_db_link." WHERE ".$table_db_link.".id_object IN (
                    SELECT ".$table_db_object.".id FROM ".$table_db_object."  ))
                    ";
        if($search == "*") {
            $select_staff = "SELECT * FROM `".$table_db."` ORDER BY fio ASC ";
        } else {
            $select_staff = "SELECT * FROM `".$table_db."` WHERE fio LIKE '".$search."%' ";
        }


        function sselect($id) {
            global $wpdb;

            $table_db_object = $wpdb->prefix.db_object;
            $table_db_link = $wpdb->prefix.db_link;
            $table_db_spec = $wpdb->prefix.db_spec;

            $select_ob = "SELECT ".$wpdb->prefix.db_link.".*, ".$table_db_object.".*, ".$table_db_spec.".*
                    FROM `".$wpdb->prefix.db_link."`
                    INNER JOIN ".$table_db_object." ON ".$table_db_object.".id = ".$table_db_link.".id_object
                    INNER JOIN ".$table_db_spec." ON ".$table_db_spec.".id = ".$table_db_link.".id_spec
                    WHERE ".$wpdb->prefix.db_link.".id_user = ".$id."";
            return $rresult = $wpdb->get_results($select_ob, ARRAY_A);
            //$wpdb->show_errors();
            //$wpdb->print_error();
        }

        $result = $wpdb->get_results($select_staff, ARRAY_A);

        //$wpdb->show_errors();
        //$wpdb->print_error();

        //http://blog.themeforest.net/tutorials/jquery-for-absolute-beginners-day-11/
        ?>
        <script type="application/javascript">
            jQuery(document).ready(function() {
                jQuery('a').hover(function(e){

                        var href = jQuery(this).attr('href');
                        jQuery('<img id="LargeImage" src="' + href + '" alt = "image" />')
                            .css('top', e.pageY)
                            .css('left', e.pageX)
                            .appendTo('body');
                    },  function() {
                        jQuery('#LargeImage').remove();
                    }
                );
            };
        </script>

        <table style="margin-top: 40px;" class="table table-condensed">
        <tbody>
        <tr class="success"><td><h6>ФИО</h6></td><td><h6>Дата рождения</h6></td><td><h6>Должность</h6></td><td><h6>Объекты</h6></td><td><h6>Годы работы</h6></td><td><h6>Доп. профессии</h6></td><td><h6>Дата обучения</h6></td><td><h6>Оценка</h6></td><td><h6>Адрес</h6></td><td><h6>Телефон</h6></td></tr>
        <?php

        foreach ($result as $row) {
            ?>
            <tr><td><a href="<?=home_url('/dosie'); ?>/index.php?view=<?=$row['id']; ?>"><?=$row['fio']; ?></a></td>
                <td><?=$row['born']; ?></td>
                <td><?=$row['position']; ?></td>
                <td><?php
                    foreach (sselect($row['id']) as $r ) {
                        if($r['object'] != "Нет данных")
                            echo $r['object']."<br />";
                    }

                    ?></td>
                <td><?=$row['experience']; ?></td>
                <td>
                    <?php
                    foreach (sselect($row['id']) as $r ) {
                        if($r['add_spec'] != "Нет данных")
                            echo $r['add_spec']."<br />";
                    }

                    ?>
                </td>
                <td><?=$row['data_learning']; ?></td>
                <td><?=$row['mark']; ?></td>
                <td><?=$row['address']; ?></td>
                <td><?=$row['phone']; ?></td>
            </tr>
        <?php
        }

    }
    ?>

    </tbody>
    </table>
<?php
}
add_shortcode('WAHOO', 'myshortcode');
