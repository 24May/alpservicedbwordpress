<?php
function Ok($atts, $content = "") {
    global $wpdb;
    $table_db_object = $wpdb->prefix.db_object;
    $table_db_spec = $wpdb->prefix.db_spec;
    $table_db_staff = $wpdb->prefix.db_staff;
    $table_db_link = $wpdb->prefix.db_link;

    $select_object = "SELECT id, object FROM `".$table_db_object."`";
    $select_spec = "SELECT id, add_spec FROM `".$table_db_spec."`";

    //SELECT DISTINCT user.fio FROM user INNER JOIN sv ON user.id = sv.id_user INNER JOIN object ON object.id = sv.id_obj WHERE  object.obj = 'Харьков' OR object.obj = 'пмк';

    echo "<h4>Подбор кадров по критериям</h4>";
    if($_POST['search-for-job']) {

        if(empty($_POST['add_spec'])) {
            ?>
            <h3 class="text-error">Для поиска надо выбрать хоть одну профессию</h3>
            <br /><br />
        <a href="<?=home_url('/select-staff'); ?>" class="btn btn-large btn-primary disabled">НАЗАД К ПОИСКУ</a>
            <?php
            exit();
        } else {
        $spec_str = implode(",", $_POST['add_spec']);


        $selectjob = "SELECT staff.fio, staff.born, staff.data_learning, staff.address, staff.phone, staff.position, object.object, spec.add_spec FROM ".$table_db_staff." as staff
                            LEFT JOIN ".$table_db_link." as link ON staff.id = link.id_user
			                LEFT JOIN ".$table_db_object." as object ON object.id = link.id_object
			                LEFT JOIN ".$table_db_spec." as spec ON spec.id = link.id_spec
			WHERE spec.id IN  (".$spec_str.")";
        $rresult = $wpdb->get_results($selectjob, ARRAY_A);
            ?>
            <table class="table table-striped">
                <caption><h4 class="text-success">подбор по специальности</h4></caption>
                <tr class="success"><td><h5>ФИО</h5></td><td><h5>дата рождения</h5></td><td><h5>Должность</h5></td><td><h5>Объекты</h5></td><td><h5>Дата обучения</h5></td><td><h5>Адрес</h5></td><td><h5>Телефон</h5></td><td><h5>Оценка</h5></td></tr>
                <tbody>
                <?php
                foreach($rresult as $row) {
                    echo "<tr><td>".$row['fio']."</td><td>".$row['born']."</td><td>".$row['add_spec']."</td><td>".$row['object']."</td><td>".$row['data_learning']."</td><td>".$row['address']."</td><td>".$row['phone']."</td><td>".$row['mark']."</td></tr>";
                }
                ?>

                </tbody>

            </table>
            <?php


        //$wpdb->show_errors();
        //$wpdb->print_error();
        }

    }
    if($_POST['search-for-object']) {
        if(empty($_POST['object'])) {
        ?>

        <h3 class="text-error">Для поиска надо выбрать хоть один объект</h3>
        <br /><br />
        <a href="<?=home_url('/select-staff'); ?>" class="btn btn-large btn-primary disabled">НАЗАД К ПОИСКУ</a>
        <?php
        exit();
        } else {
            $object_str = implode(",", $_POST['object']);


            $selectjob = "SELECT staff.fio, staff.born, staff.data_learning, staff.address, staff.phone, staff.position, object.object, spec.add_spec, staff.mark FROM ".$table_db_staff." as staff
                            LEFT JOIN ".$table_db_link." as link ON staff.id = link.id_user
			                LEFT JOIN ".$table_db_object." as object ON object.id = link.id_object
			                LEFT JOIN ".$table_db_spec." as spec ON spec.id = link.id_spec
			WHERE object.id IN  (".$object_str.")";
            $rresult = $wpdb->get_results($selectjob, ARRAY_A);
            ?>
            <table class="table table-striped">
                <caption><h4 class="text-success">подбор по объекту</h4></caption>
                <tbody><tr class="success"><td><h5>ФИО</h5></td><td><h5>дата рождения</h5></td><td><h5>Должность</h5></td><td><h5>Объекты</h5></td><td><h5>Дата обучения</h5></td><td><h5>Адрес</h5></td><td><h5>Телефон</h5></td><td><h5>Оценка</h5></td></tr>
                <?php
                foreach($rresult as $row) {
                    echo "<tr><td>".$row['fio']."</td><td>".$row['born']."</td><td>".$row['position']."</td><td>".$row['object']."</td><td>".$row['data_learning']."</td><td>".$row['address']."</td><td>".$row['phone']."</td><td>".$row['mark']."</td></tr>";
                }
                ?>

                </tbody>

            </table>
            <?php

        }
    }
    if($_POST['search-for-mark']) {
        if(empty($_POST['mark'])) {
        ?>
        <h3 class="text-error">Для поиска надо задать оценку</h3>
        <br /><br />
        <a href="<?=home_url('/select-staff'); ?>" class="btn btn-large btn-primary disabled">НАЗАД К ПОИСКУ</a>
        <?php
        exit();
        } else {
            $selectmark = "SELECT staff.fio, staff.born, staff.data_learning, staff.address, staff.phone, staff.position, staff.mark FROM ".$table_db_staff." as staff

			WHERE staff.mark IN  (".$_POST['mark'].")";
            $rresult = $wpdb->get_results($selectmark, ARRAY_A);

            ?>
            <table class="table table-striped">
                <caption><h4 class="text-success">подбор по оценке</h4></caption>
                <tbody>
                <tr class="success"><td><h5>ФИО</h5></td><td><h5>дата рождения</h5></td><td><h5>Должность</h5></td><td><h5>Объекты</h5></td><td><h5>Дата обучения</h5></td><td><h5>Адрес</h5></td><td><h5>Телефон</h5></td><td>Оценка</td></tr>
                <?php
                foreach($rresult as $row) {
                    echo "<tr><td>".$row['fio']."</td><td>".$row['born']."</td><td>".$row['position']."</td><td>".$row['object']."</td><td>".$row['data_learning']."</td><td>".$row['address']."</td><td>".$row['phone']."</td><td>".$row['mark']."</td></tr>";
                }
                ?>

                </tbody>

            </table>
        <?php

        }
    }
    if($_POST['hochu-brigadu']) {
        if(empty($_POST['object']) or empty($_POST['add_spec']) or empty($_POST['mark']) ) {
            ?>
            <h3 class="text-error">КРИТЕРИИ ПОИСКА НЕ ЗАДАНЫ</h3>
            <br /><br /><br />
        <a href="<?=home_url('/select-staff'); ?>" class="btn btn-large btn-primary disabled">НАЗАД К ПОИСКУ</a><?php
            exit();
        }

        $object_str = implode(",", $_POST['object']);
        $spec_str = implode(",", $_POST['add_spec']);

        $select_s ="SELECT DISTINCT ".$table_db_staff.".fio, ".$table_db_staff.".*, ".$table_db_object.".*, ".$table_db_spec.".* FROM ".$table_db_staff."
                LEFT  JOIN ".$table_db_link." ON ".$table_db_staff.".id = ".$table_db_link.".id_user
                LEFT   JOIN ".$table_db_object." ON ".$table_db_object.".id = ".$table_db_link.".id_object
                LEFT   JOIN ".$table_db_spec." ON ".$table_db_spec.".id = ".$table_db_link.".id_spec
                WHERE ".$table_db_staff.".mark = ".$_POST['mark']." AND ".$table_db_object.".id IN (".$object_str.") AND ".$table_db_spec.".id IN (".$spec_str.") ";

        $select_ss = "SELECT staff.fio, COUNT(*) AS cnt  FROM ".$table_db_staff." as staff

			LEFT JOIN ".$table_db_link." as link ON staff.id = link.id_user
			LEFT JOIN ".$table_db_object." as object ON object.id = link.id_object
			LEFT JOIN ".$table_db_spec." as spec ON spec.id = link.id_spec

			WHERE staff.mark = ".$_POST['mark']." AND object.id IN (".$object_str.") AND  spec.id IN (".$spec_str.")
                        GROUP BY staff.id
                        HAVING cnt = 1";



        $rresult = $wpdb->get_results($select_ss, ARRAY_A);

        //$wpdb->show_errors();
        //$wpdb->print_error();

        ?>
        <table>

        <?php
        foreach($rresult as $row) {
            echo "<tr class='warning'><td>".$row['fio']."</td></tr>";
        }
        ?>
        </table>
        <?php
    }


    $data_spec = $wpdb->get_results($select_spec, ARRAY_A);
    $data_object = $wpdb->get_results($select_object, ARRAY_A);

    //$wpdb->show_errors();
    //$wpdb->print_error();
    ?>
    <form action="" method="post" >
        <fieldset>
        <table>
            <tr class="success"><td><h5>Выбор объекта</h5></td><td><h5>Выбор профессии</h5></td><td><h5>Оценка прораба, мастера, нач. уч.</h5></td></tr>
            <tr><td>
                    <?php
                    foreach ($data_object as $row_object) {
                        ?>
                        <?php if($row_object['id'] != 1) : ?>
                            <label class="checkbox">
                                <input type="checkbox" name="object[]" value="<?=$row_object['id']; ?>">
                                <?=$row_object['object']; ?>

                            </label>
                        <?php endif; ?>
                    <?php
                    }
                    ?>
                </td>
                <td>
                    <?php
                    foreach ($data_spec as $row_spec) {
                        ?>
                        <?php if($row_spec['id'] != 1) : ?>
                            <label class="checkbox">
                                <input type="checkbox" name="add_spec[]" value="<?=$row_spec['id']; ?>">
                                <?=$row_spec['add_spec']; ?>
                            </label>
                        <?php endif; ?>
                    <?php
                    }
                    ?>
                </td>
                <td><input type="text"  name="mark" placeholder="Введите оценку "></td>
            </tr>
            <tr class="success"><td style="text-align: center;" ><input type="submit" class="btn btn-info" name="search-for-job" value="Подобрать по профессиям" /></td><td style="text-align: center;" ><input type="submit" class="btn btn-info" name="search-for-object" value="Подобрать по объекту" /></td><td style="text-align: center;" ><input type="submit" class="btn btn-info" name="search-for-mark" value="Подобрать по оценке" /></td></tr>
            <tr class="warning" ><td colspan="3" style="text-align: center;"><input type="submit" class="btn btn-info" name="hochu-brigadu" value="Подобрать по всем критериям" /></td></tr>
        </table>
        </fieldset>
    </form>
<?php
}
add_shortcode('STAFF-SELECT', 'Ok');
