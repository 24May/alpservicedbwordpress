<?php
/*
    Plugin Name: Database Staff
    Description: База данных сотрудников
    Version: 1.0
    Author: Andrey Kopitsa
    Author URI: http://24may.org.ua/
    Plugin URI: http://easy-code.ru/lesson/building-wordpress-plugin-part-one
*/
/*
 * Для того, чтобы после активации плагина, в меню «Параметры», появилась ссылка на меню настроек нашего плагина,
 * мы напишем функцию db_staff_admin_menu внутри, которой выполним команду add_options_page, передав необходимые параметры.
 * SELECT user.fio, object.obj FROM user INNER JOIN sv ON user.id = sv.id_user INNER JOIN object ON object.id = sv.id_obj WHERE object.obj = 'Запорожье';
 * SELECT DISTINCT user.fio FROM user INNER JOIN sv ON user.id = sv.id_user INNER JOIN object ON object.id = sv.id_obj WHERE  object.obj = 'Харьков' OR object.obj = 'пмк';
 *  */
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }
define('WPDB', 1);
///////////////////////////////////

function hkdc_admin_styles() {
    wp_enqueue_style( 'jquery-ui-datepicker-style' , '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');
}
add_action('admin_print_styles', 'hkdc_admin_styles');
function hkdc_admin_scripts() {
    wp_deregister_script('jquery');
    wp_enqueue_script( 'jquery-ui-datepicker-iu', '//code.jquery.com/ui/1.10.4/jquery-ui.js' );
    $pluginfolder = get_bloginfo('url') . '/' . PLUGINDIR . '/' . dirname(plugin_basename(__FILE__));
    wp_enqueue_script('my_js', $pluginfolder.'/js/admin.js');
}
add_action('admin_print_scripts', 'hkdc_admin_scripts');


function SelectObject() {
    global $wpdb;
    $table_db = $wpdb->prefix.db_object;
    $select = "SELECT object FROM ".$table_db."";
    $data = $wpdb->get_results($select, ARRAY_A);
    echo "<h3>Объекты внесённые в базу</h3>";


    foreach ($data as $row ) {
        if($row['object'] != "Нет данных")
        echo "<code>".$row['object']. "</code>";
    }
}
function SelectSpec() {
    global $wpdb;
    $table_db = $wpdb->prefix.db_spec;
    $select = "SELECT add_spec FROM ".$table_db."";
    $data = $wpdb->get_results($select, ARRAY_A);
    echo "<h3>Профессии внесённые в базу</h3>";


    foreach ($data as $row ) {
        if($row['add_spec'] != "Нет данных")
        echo "<code>".$row['add_spec']. "</code>";
    }
}

function db_staff_admin_menu(){
    //Добавляем меню плагина в админке "настройки"
    add_options_page('Сотрудники', 'Сотрудники', 8, basename(__FILE__), 'create_db_staff');
    //Добавляем многоуровневое меню в колонке
    add_menu_page('Заполнение базы данных сотрудников', 'Сотрудники', 'read', 'theme-options', 'wps_theme_func');
    add_submenu_page( 'theme-options', 'Сотрудники', 'Данные сотрудников', 'read', 'theme-op-settings', 'wps_theme_func_settings');
    add_submenu_page( 'theme-options', 'Ввод дополнительных данных', 'Ввод дополнительных данных', 'read', 'theme-op-faq', 'wps_theme_func_faq');
}
//это подробное о сотруднике
function dosieshortcode($atts, $content = "") {
    if($_GET['view']) {
    global $wpdb;

    $table_db_staff = $wpdb->prefix.db_staff;
    $table_db_object = $wpdb->prefix.db_object;
    $table_db_link = $wpdb->prefix.db_link;
    $table_db_spec = $wpdb->prefix.db_spec;

    $select_s ="SELECT  ".$table_db_staff.".*, ".$table_db_object.".*, ".$table_db_spec.".* FROM ".$table_db_staff."
                INNER JOIN ".$table_db_link." ON ".$table_db_staff.".id = ".$table_db_link.".id_user
                INNER JOIN ".$table_db_object." ON ".$table_db_object.".id = ".$table_db_link.".id_object
                INNER JOIN ".$table_db_spec." ON ".$table_db_spec.".id = ".$table_db_link.".id_spec
                WHERE ".$table_db_staff.".id = ".$_GET['view']."";

    $rresult = $wpdb->get_results($select_s, ARRAY_A);
    ?>
    <h3><?=$rresult[0]['fio']; ?></h3>
    <table class="table table-striped">
        <tr class="warning"><td rowspan="4"><img src="<?php if(!$rresult[0]['photo']) {
                    echo WP_CONTENT_URL."/uploads/photo/noimage.jpg";
                } else {
                    echo $rresult[0]['photo'];
                }
                ?>" /></td><td class="text-success"><strong>ФИО : </strong><?=$rresult[0]['fio']; ?></td></tr>
        <tr  class="warning"><td class="text-success"><strong>Дата Рождения : </strong><?=$rresult[0]['born']; ?></td></tr>
        <tr class="warning"><td class="text-success"><strong>Должность : </strong><?=$rresult[0]['position']; ?></td></tr>
        <tr class="warning"><td class="text-success"><strong>ВУЗ : </strong><?=$rresult[0]['education']; ?></td></tr>
        <tr  class="warning"><td></td><td class="text-success" style="right: 50%; " ><strong>Специальность : </strong><?=$rresult[0]['spec']; ?></td></tr>
        <tr class="warning"><td></td><td class="text-success" ><?=$rresult[0]['doc']." "; ?><?=$rresult[0]['series']." "; ?><?=$rresult[0]['number']; ?></td></tr>
        <tr class="warning"><td></td><td  class="text-success"><strong>ИНН : </strong><?=$rresult[0]['inn']; ?></td></tr>
        <tr class="warning"><td></td><td class="text-success"><strong>Принят : </strong><?=$rresult[0]['start_job']; ?></td></tr>
        <tr class="warning"><td></td><td class="text-success"><strong>Уволен : </strong><?=$rresult[0]['finish_job']; ?></td></tr>
        <tr class="warning"><td></td><td class="text-success"><strong>Опыт работы в Альпсервисе : </strong><?=$rresult[0]['experience']; ?></td></tr>
        <tr class="warning"><td></td><td class="text-success"><strong>Адрес : </strong><?=$rresult[0]['address']; ?></td></tr>
        <tr class="warning"><td></td><td class="text-success"><strong>Телефон : </strong><?=$rresult[0]['phone']; ?></td></tr>
    </table>

    <?php
    } else {
        echo "tes";
    }
}
add_shortcode('dosie', 'dosieshortcode');
//подключаем файл оповещений
include_once "info.php";

include_once "search-staff.php";

include_once "staff-select.php";



function db_install()
{
    global $wpdb;
    $table_db = $wpdb->prefix.db_staff;
    $sql1 =
        "
        CREATE TABLE IF NOT EXISTS `".$table_db."` (
                `id` int(10) NOT NULL AUTO_INCREMENT,
                `table_number` varchar(20) NOT NULL,
                `fio` varchar(255) NOT NULL,
                `sex` ENUM('M','F') DEFAULT 'M' NOT NULL,
                `born` date NOT NULL,
                `spec` varchar(255) NOT NULL,
                `education` varchar(255) NOT NULL,
                `position` varchar(255) NOT NULL,
                `inn` varchar(15) NOT NULL,
                `doc` varchar(25) NOT NULL,
                `date_doc` varchar(255) NOT NULL,
                `who_doc` varchar(255) NOT NULL,
                `number` varchar(15) NOT NULL,
                `series` char(3) NOT NULL,
                `start_job` date NOT NULL,
                `finish_job` date NOT NULL,
                `child` varchar(255) NOT NULL,
                `date_child` varchar(255) NOT NULL,
                `order_start_job` varchar(255) NOT NULL,
                `order_finish_job` varchar(255) NOT NULL,
                `invalid` ENUM('Y','N') DEFAULT 'N' NOT NULL,
                `id_edu` varchar(255) NOT NULL,
                `id_family` varchar(255) NOT NULL,
                `phone` varchar(50) NOT NULL
                PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ";
    $wpdb->query($sql1);



    $sql1 =
        "
        CREATE INDEX i_fio ON `".$wpdb->prefix.db_staff."` (fio,table_number,id,id_edu,id_family )
        ";
    $wpdb->query($sql1);



    //$wpdb->show_errors();
    //$wpdb->print_error();
}

function db_uninstall()
{
    global $wpdb;
    $table_db = $wpdb->prefix.db_staff;
    $sql1 = "DROP TABLE `".$table_db."`;";
    $wpdb->query($sql1);



}
//register_activation_hook( __FILE__, 'db_install');
//register_deactivation_hook( __FILE__, 'db_uninstall');
add_action('admin_menu', 'db_staff_admin_menu');


function create_db_staff() {
?>

<div class="wrap">
    <h2>База данных сотрудников</h2>
    <form method="post" action="" enctype="multipart/form-data">
        <h3>Выполнить загрузку сотрудников из 1С </h3>
        <em>Для обновления каталога, выберите фаил с расширением .csv, расположенного на вашем компьютере.<em/>
        <!-- <input type="hidden" name="MAX_FILE_SIZE" value="2048" /> -->
        <br/><input type="file"   name="filecsv" />
        <p class="submit"><input name="submit_csv" id="submit" class="button button-primary" value="Загрузить файл" type="submit"></p>
    </form>
    <?php
    if($_REQUEST['submit_csv']) {
         include "parser.php";
    }


    ?>
    </div>

    <div class="wrap">

        <form method="post" action="" enctype="multipart/form-data">
            <h3>Обновить сотрудников из 1С </h3>
            <em>Для обновления каталога, выберите фаил с расширением .csv, расположенного на вашем компьютере.<em/>
                <!-- <input type="hidden" name="MAX_FILE_SIZE" value="2048" /> -->
                <br/><input type="file"   name="filecsv1" />
                <p class="submit"><input name="submit_csv1" id="submit1" class="button button-primary" value="Загрузить файл" type="submit"></p>
        </form>
        <?php
        if($_REQUEST['submit_csv1']) {
            include "parser.php";
        }


        ?>
    </div>

<?php
}
// Функция вызывается при переходе на ссылку по 20-ой строке кода этого файла
function wps_theme_func_faq() {
    ?>
    <h2>Ввод дополнительных данных</h2>
    <p style="color: red"><strong>До начала добавления данных в базу, необходимо удостовериться что её нет в системе. Внизу поля для ввода можно видеть данные, которые уже есть в системе. </strong></p>
    <p style="color: red; font-size: 12px;">НЕ СОЗДАВАЙТЕ ДУБЛИКАТОВ ИЛИ СХОЖИХ ДАННЫХ ИБО ПРИ ВЫБОРКЕ, ВЫ ПОЛУЧИТЕ НЕПОЛНЫЕ ДАННЫЕ</p>
    <hr />
    <p style="color: #008000"><strong>Ввод объектов</strong></p>
    <form method="post" action="" >
        <input type="text" name="object_name" />
        <input type="submit" name="submit_object_name" class="button button-primary" value="Записать" />
    </form>
<?php
    if($_REQUEST['submit_object_name']) {
        $object_name = mb_strtolower((trim($_POST['object_name'])),'utf-8');
        global $wpdb;
        $table_db = $wpdb->prefix.db_object;
        $select = "SELECT object FROM ".$table_db."";
        $data = $wpdb->get_results($select, ARRAY_A);
        $ar = array();
        foreach ($data as $value) {
            $ar[] = mb_strtolower($value['object'],'utf-8');
        }
        if(in_array($object_name, $ar) and $object_name != "") {
            echo "<strong>Данный объект присутствует в базе данных</strong>";
            SelectObject ();

        } else {

            $insert = "INSERT INTO ".$table_db."  VALUES (null, '".trim($_POST['object_name'])."')";
            $wpdb->query($insert);
            echo "<strong>Объект записан</strong>";
            SelectObject();

        }

    } else {
        SelectObject();
    }
    ?>

    <hr />
    <p style="color: #008000"><strong>Ввод дополнительных профессий</strong></p>
    <form method="post" action="" >
        <input type="text" name="spec_name" />
        <input type="submit" name="submit_spec_name" class="button button-primary" value="Записать" />
    </form>
    <?php
    if($_REQUEST['submit_spec_name']) {
        $spec_name = mb_strtolower((trim($_POST['spec_name'])),'utf-8');
        global $wpdb;
        $table_db = $wpdb->prefix.db_spec;
        $select = "SELECT add_spec FROM ".$table_db."";
        $data = $wpdb->get_results($select, ARRAY_A);
        $ar = array();
        foreach ($data as $value) {
            $ar[] = mb_strtolower($value['add_spec'],'utf-8');
        }
        if(in_array($spec_name, $ar) and $spec_name != "") {
            echo "<strong>Данный профессия присутствует в базе данных</strong>";
            SelectSpec();

        } else {

            $insert = "INSERT INTO ".$table_db."  VALUES (null, '".trim($_POST['spec_name'])."')";
            $wpdb->query($insert);
            echo "<strong>Профессия добавлена</strong>";
            SelectSpec();
        }

    } else {

        SelectSpec();
    }
}
function wps_theme_func() {
?>
    <h2>Плагин для ведения учёта сотрудников и их данных</h2>
    <?php
    date_default_timezone_set('Europe/Kyiv');
    $connect_db = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
    mysql_query("SET NAMES 'utf8'");
    mysql_select_db("wpdb", $connect_db);
    $posts = mysql_query("SELECT fio, born FROM wp_db_staff", $connect_db);


    $num_rows = mysql_num_rows( $posts );
    echo "<h4>Количество сотрудников в базе данных : $num_rows</h4>";



}
include "select-data.php";
