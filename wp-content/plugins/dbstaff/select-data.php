<?php
if(!defined("WPDB")) die("Silence is golden.");
function wps_theme_func_settings() {
    if($_GET['add'] and $_GET['add'] == "new") {
        include_once("includes/add_staff.php");
        exit();
    }
    if($_GET['edit'] and $_GET['edit'] != "") {
        include_once("includes/edit_staff.php");
        exit();
    }
    if($_GET['delete'] and $_GET['delete'] != "" ) {
        global $wpdb;
        $table_db = $wpdb->prefix.db_staff;
        if($_GET['result'] and $_GET['result'] == "yes") {
            $wpdb->delete( $table_db, array( 'id' => $_GET['delete']));
            $wpdb->show_errors();
            $wpdb->print_error();
            exit("<h1 style='color: red'>!!! ЗАПИСЬ УДАЛЕНА ИЗ БАЗЫ ДАННЫХ !!!</h1><a style='color: #008000; font-size: 16px; font-weight: bold' href=$_SERVER[PHP_SELF]?page=theme-op-settings>НАЗАД К СПИСКУ</a>");
        }
        $select = "SELECT * FROM ".$table_db." WHERE id = '".$_GET['delete']."'";
        $data = $wpdb->get_results($select, ARRAY_A);
        echo <<<EOT
<h2 style='color: red;'><strong>ИНИЦИИРОВАНО УДАЛЕНИЕ СОТРУДНИКА</strong></h2>
<h3 style='color: red;'>ПРОВЕРЬТЕ ДАННЫЕ ПЕРЕД ТЕМ КАК ПОДТВЕРДИТЬ УДАЛЕНИЕ</h3>
EOT;
        echo "<table style='margin-top: 40px'>";
        foreach ($data as $row ) {
            printf("<tr><td><span style='color: #008000; font-weight: bold;'> Фамилия Имя Отчество: </span></td><td><span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'> Табельный номер: </span></td><td> <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Дата рождения: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Специальность: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Образование: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Вид образования: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Должность: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Телефон: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>ИНН: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Документ: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Серия: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Номер: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Кем выдан: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Дата выдачи: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Начало работы: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Конец работы: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Семейный статус: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Дети: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Дата рождения: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Номер приказа приема: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>
            <tr><td><span style='color: #008000; font-weight: bold;'>Номер приказа увольнения: </span></td><td>  <span style='font-size: 16px';>%s</span><td></tr>",
            $row['fio'], $row['spec'], $row['born'], $row['spec'], $row['education'], $row['id_edu'], $row['position'], $row['phone'], $row['inn'], $row['doc'],
            $row['series'], $row['number'],$row['who_doc'], $row['date_doc'], $row['start_job'], $row['finish_job'], $row['id_famaly'], $row['child'], $row['date_child'],
            $row['order_start_job'], $row['order_finish_job']);
        }
        echo '<tr><td><a href='.$_SERVER[PHP_SELF].'?page=theme-op-settings>НАЗАД К СПИСКУ</a> </td><td><a style="color: red; font-size: 16px;" href='.$_SERVER[PHP_SELF].'?page=theme-op-settings&delete='.$row[id].'&result=yes>!!! УДАЛИТЬ !!!</a> </td></tr></table>';
        exit();
    }
    function SelectStaff () {
        global $wpdb;
        $table_db = $wpdb->prefix.db_staff;
        if($_POST['search_staff'] and $_POST['search_staff'] != "") {
            $search = trim($_POST['search_staff']);
            $select = "SELECT id, spec, position, phone, fio, born FROM ".$table_db." WHERE fio LIKE '".$search."%' ";
        } else {
            $select = "SELECT id, spec, position, phone, fio, born FROM ".$table_db." ORDER BY fio ASC";
        }

        $data = $wpdb->get_results($select, ARRAY_A);

        foreach ($data as $row ) {
            ?>
            <tr id="post-1" class="post-1 type-post status-publish format-standard hentry category-- alternate iedit author-self level-0">
                <th scope="row" class="check-column">
                    <label class="screen-reader-text" for="cb-select-1"></label>
                    <input id="cb-select-1" name="post[]" value="1" type="checkbox">
                    <div class="locked-indicator"></div>
                </th>

                <td class="post-title page-title column-title"><strong><a class="row-title" href="http://db.dev.24may.org.ua/wp-admin/post.php?post=1&amp;action=edit" title="Редактировать «<?php echo $row['fio']; ?>»"><?php echo $row['fio']; ?></a></strong>

                    <div class="row-actions"><span class="edit"><a href="<?=$_SERVER['PHP_SELF']; ?>?page=theme-op-settings&edit=<?=$row['id'];?>" title="Редактировать этот элемент">Изменить</a> | </span><span class="inline hide-if-no-js"><a href="#" class="editinline" title="Редактировать свойства этого элемента">Свойства</a> | </span><span class="trash"><a class="submitdelete" title="Переместить этот элемент в корзину" href="<?=$_SERVER['PHP_SELF']."?page=theme-op-settings&delete=".$row['id']; ?>">Удалить</a> | </span><span class="view"><a href="http://db.dev.24may.org.ua/2014/05/30/%d0%bf%d1%80%d0%b8%d0%b2%d0%b5%d1%82-%d0%bc%d0%b8%d1%80/" title="«<?php echo $row['fio']; ?>»" rel="permalink">Перейти</a></span></div>
                </td>


                <td class="author column-author"><?= $row['spec']; ?></td>
                <td class="categories column-categories"><?=$row['phone']; ?></td><td class="tags column-tags"><?=$row['position']; ?></td>			<td class="comments column-comments"><div class="post-com-count-wrapper">
                        <a href="http://db.dev.24may.org.ua/wp-admin/edit-comments.php?p=1" title="0 ожидающих" class="post-com-count"><span class="comment-count"><?=$row['id']; ?></span></a>			</div></td>
                <td class="date column-date"><abbr title="30.05.2014 10:35:51"><?=$row['born']; ?></abbr></td>
            </tr>
            <?php
        }
    }

?>
<h3>Страница ручного ввода и редактирования данных</h3>
<div id="wpadminbar" class="" role="navigation">
    <a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Перейти к верхней панели</a>
    <div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Верхняя панель инструментов." tabindex="0">
        <ul id="wp-admin-bar-root-default" class="ab-top-menu">
            <li id="wp-admin-bar-menu-toggle"><a aria-expanded="false" class="ab-item" href="#"><span class="ab-icon"></span><span class="screen-reader-text">Меню</span></a>		</li>
            <li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://db.dev.24may.org.ua/wp-admin/about.php" title="О WordPress"><span class="ab-icon"></span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
                        <li id="wp-admin-bar-about"><a class="ab-item" href="http://db.dev.24may.org.ua/wp-admin/about.php">О WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
                        <li id="wp-admin-bar-wporg"><a class="ab-item" href="http://ru.wordpress.org/">WordPress.org</a>		</li>
                        <li id="wp-admin-bar-documentation"><a class="ab-item" href="http://codex.wordpress.org/Заглавная_страница">Документация</a>		</li>
                        <li id="wp-admin-bar-support-forums"><a class="ab-item" href="http://ru.forums.wordpress.org/">Форумы поддержки</a>		</li>
                        <li id="wp-admin-bar-feedback"><a class="ab-item" href="http://ru.forums.wordpress.org/forum/20">Обратная связь</a>		</li></ul></div>		</li>
            <li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://db.dev.24may.org.ua/">База данных сотрудников ЧАО "Альпсе…</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
                        <li id="wp-admin-bar-view-site"><a class="ab-item" href="http://db.dev.24may.org.ua/">Перейти на сайт</a>		</li></ul></div>		</li>
            <li id="wp-admin-bar-comments"><a class="ab-item" href="http://db.dev.24may.org.ua/wp-admin/edit-comments.php" title="0 комментариев ожидают проверки"><span class="ab-icon"></span><span id="ab-awaiting-mod" class="ab-label awaiting-mod pending-count count-0">0</span></a>		</li>
            <li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://db.dev.24may.org.ua/wp-admin/post-new.php" title="Добавить"><span class="ab-icon"></span><span class="ab-label">Добавить</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
                        <li id="wp-admin-bar-new-post"><a class="ab-item" href="http://db.dev.24may.org.ua/wp-admin/post-new.php">Запись</a>		</li>
                        <li id="wp-admin-bar-new-media"><a class="ab-item" href="http://db.dev.24may.org.ua/wp-admin/media-new.php">Медиафайл</a>		</li>
                        <li id="wp-admin-bar-new-page"><a class="ab-item" href="http://db.dev.24may.org.ua/wp-admin/post-new.php?post_type=page">Страницу</a>		</li>
                        <li id="wp-admin-bar-new-user"><a class="ab-item" href="http://db.dev.24may.org.ua/wp-admin/user-new.php">Пользователя</a>		</li></ul></div>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
            <li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="http://db.dev.24may.org.ua/wp-admin/profile.php" title="Моя учётная запись">Привет,<?php global $user_identity; echo $user_identity; ?><img alt="" src="http://0.gravatar.com/avatar/278fa50cdc14be28fb0c5dd50e75a84f?s=26&amp;d=&amp;r=G" class="avatar avatar-26 photo" height="26" width="26"></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
                        <li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://db.dev.24may.org.ua/wp-admin/profile.php"><img alt="" src="http://0.gravatar.com/avatar/278fa50cdc14be28fb0c5dd50e75a84f?s=64&amp;d=&amp;r=G" class="avatar avatar-64 photo" height="64" width="64"><span class="display-name">andrey</span></a>		</li>
                        <li id="wp-admin-bar-edit-profile"><a class="ab-item" href="#">Изменить профиль</a>		</li>
                        <li id="wp-admin-bar-logout"><a class="ab-item" href="http://db.dev.24may.org.ua/wp-login.php?action=logout&amp;_wpnonce=ac9b6eaf77">Выйти</a>		</li></ul></div>		</li></ul>			</div>
    <a class="screen-reader-shortcut" href="http://db.dev.24may.org.ua/wp-login.php?action=logout&amp;_wpnonce=ac9b6eaf77">Выйти</a>
</div>


<div id="wpbody">

<div id="wpbody-content" aria-label="Основное содержимое" tabindex="0">

<div class="wrap">
<h2>СОТРУДНИКИ<a href="<?=$_SERVER['PHP_SELF']; ?>?page=theme-op-settings&add=new" class="add-new-h2">Добавить нового</a></h2>

<form  action="" method="post">

    <p class="search-box">
        <label class="screen-reader-text" for="post-search-input">Поиск сотрудника:</label>
        <input id="post-search-input" name="search_staff" value="" type="search" />
        <input name="" id="search-submit" class="button button-primary" value="Поиск сотрудника" type="submit" /></p>
</form>

        <br class="clear">
    </div>
    <table class="wp-list-table widefat fixed posts">
        <thead>
        <tr>
            <th scope="col" id="cb" class="manage-column column-cb check-column" style=""><label class="screen-reader-text" for="cb-select-all-1">Выделить все</label><input id="cb-select-all-1" type="checkbox"></th><th scope="col" id="title" class="manage-column column-title sortable desc" style=""><a href="http://db.dev.24may.org.ua/wp-admin/edit.php?orderby=title&amp;order=asc"><span>Фамилия Имя Отчество</span><span class="sorting-indicator"></span></a></th><th scope="col" id="author" class="manage-column column-author" style="">Специальность по диплому</th><th scope="col" id="categories" class="manage-column column-categories" style="">Телефон</th><th scope="col" id="tags" class="manage-column column-tags" style="">Профессия</th><th scope="col" id="comments" class="manage-column column-comments num sortable desc" style=""><a href="http://db.dev.24may.org.ua/wp-admin/edit.php?orderby=comment_count&amp;order=asc"><span><span class="vers"><span title="Комментарии"></span>№</span></span><span class="sorting-indicator"></span></a></th><th scope="col" id="date" class="manage-column column-date sortable asc" style=""><a href="http://db.dev.24may.org.ua/wp-admin/edit.php?orderby=date&amp;order=desc"><span>Дата рождения</span><span class="sorting-indicator"></span></a></th>	</tr>
        </thead>

        <tfoot>
        <tr>
            <th scope="col" class="manage-column column-cb check-column" style=""><label class="screen-reader-text" for="cb-select-all-2">Выделить все</label><input id="cb-select-all-2" type="checkbox"></th><th scope="col" class="manage-column column-title sortable desc" style=""><a href="http://db.dev.24may.org.ua/wp-admin/edit.php?orderby=title&amp;order=asc"><span>Фамилия Имя Отчество</span><span class="sorting-indicator"></span></a></th><th scope="col" class="manage-column column-author" style="">Специальность по диплому</th><th scope="col" class="manage-column column-categories" style="">Телефон</th><th scope="col" class="manage-column column-tags" style="">Профессия</th><th scope="col" class="manage-column column-comments num sortable desc" style=""><a href="http://db.dev.24may.org.ua/wp-admin/edit.php?orderby=comment_count&amp;order=asc"><span><span class="vers"><span title="Комментарии" ></span>№</span></span><span class="sorting-indicator"></span></a></th><th scope="col" class="manage-column column-date sortable asc" style=""><a href="http://db.dev.24may.org.ua/wp-admin/edit.php?orderby=date&amp;order=desc"><span>Дата рождения</span><span class="sorting-indicator"></span></a></th>	</tr>
        </tfoot>

        <tbody id="the-list">
        <!---- start --->
        <?php SelectStaff(); ?>
        <!---- end  --->
        </tbody>
    </table>
    <div class="tablenav bottom">

        <div class="alignleft actions bulkactions">
            <select name="action2">
                <option value="-1" selected="selected">Действия</option>
                <option value="edit" class="hide-if-no-js">Изменить</option>
                <option value="trash">Удалить</option>
            </select>
            <input name="" id="doaction2" class="button action" value="Применить" type="submit">
        </div>
        <div class="alignleft actions">
        </div>
        <div class="tablenav-pages one-page"><span class="displaying-num">1 элемент</span>
<span class="pagination-links"><a class="first-page disabled" title="Перейти на первую страницу" href="http://db.dev.24may.org.ua/wp-admin/edit.php">«</a>
<a class="prev-page disabled" title="Перейти на предыдущую страницу" href="http://db.dev.24may.org.ua/wp-admin/edit.php?paged=1">‹</a>
<span class="paging-input">1 из <span class="total-pages">1</span></span>
<a class="next-page disabled" title="Перейти на следующую страницу" href="http://db.dev.24may.org.ua/wp-admin/edit.php?paged=1">›</a>
<a class="last-page disabled" title="Перейти на последнюю страницу" href="http://db.dev.24may.org.ua/wp-admin/edit.php?paged=1">»</a></span></div>
        <br class="clear">
    </div>




<form method="get" action=""><table style="display: none"><tbody id="inlineedit">

<tr id="inline-edit" class="inline-edit-row inline-edit-row-post inline-edit-post quick-edit-row quick-edit-row-post inline-edit-post" style="display: none"><td colspan="7" class="colspanchange">

        <fieldset class="inline-edit-col-left"><div class="inline-edit-col">
                <h4>Свойства</h4>

                <label>
                    <span class="title">Фамилия Имя Отчество</span>
                    <span class="input-text-wrap"><input name="post_title" class="ptitle" value="" type="text"></span>
                </label>

                <label>
                    <span class="title">Ярлык</span>
                    <span class="input-text-wrap"><input name="post_name" value="" type="text"></span>
                </label>


                <label><span class="title">Дата</span></label>
                <div class="inline-edit-date">
                    <div class="timestamp-wrap"><select name="mm">
                            <option value="01">01-Янв</option>
                            <option value="02">02-Фев</option>
                            <option value="03">03-Мар</option>
                            <option value="04">04-Апр</option>
                            <option value="05" selected="selected">05-Май</option>
                            <option value="06">06-Июн</option>
                            <option value="07">07-Июл</option>
                            <option value="08">08-Авг</option>
                            <option value="09">09-Сен</option>
                            <option value="10">10-Окт</option>
                            <option value="11">11-Ноя</option>
                            <option value="12">12-Дек</option>
                        </select> <input name="jj" value="30" size="2" maxlength="2" autocomplete="off" type="text">, <input name="aa" value="2014" size="4" maxlength="4" autocomplete="off" type="text"> в <input name="hh" value="10" size="2" maxlength="2" autocomplete="off" type="text"> : <input name="mn" value="35" size="2" maxlength="2" autocomplete="off" type="text"></div><input id="ss" name="ss" value="51" type="hidden">			</div>
                <br class="clear">

                <label class="inline-edit-author"><span class="title">Специальность по димплому</span><select name="post_author" class="authors">
                        <option value="1">andrey</option>
                    </select></label>
                <div class="inline-edit-group">
                    <label class="alignleft">
                        <span class="title">Пароль</span>
                        <span class="input-text-wrap"><input name="post_password" class="inline-edit-password-input" value="" type="text"></span>
                    </label>

                    <em style="margin:5px 10px 0 0" class="alignleft">
                        –ИЛИ–				</em>
                    <label class="alignleft inline-edit-private">
                        <input name="keep_private" value="private" type="checkbox">
                        <span class="checkbox-title">Личное</span>
                    </label>
                </div>


            </div></fieldset>


        <fieldset class="inline-edit-col-center inline-edit-categories"><div class="inline-edit-col">


                <span class="title inline-edit-categories-label">Рубрики</span>
                <input name="post_category[]" value="0" type="hidden">
                <ul class="cat-checklist category-checklist">

                    <li id="category-1" class="popular-category"><label class="selectit"><input value="1" name="post_category[]" id="in-category-1" type="checkbox"> Без рубрики</label></li>
                </ul>


            </div></fieldset>


        <fieldset class="inline-edit-col-right"><div class="inline-edit-col">



                <label class="inline-edit-tags">
                    <span class="title">Метки</span>
                    <textarea cols="22" rows="1" name="tax_input[post_tag]" class="tax_input_post_tag"></textarea>
                </label>




                <div class="inline-edit-group">
                    <label class="alignleft">
                        <input name="comment_status" value="open" type="checkbox">
                        <span class="checkbox-title">Разрешить комментарии</span>
                    </label>
                    <label class="alignleft">
                        <input name="ping_status" value="open" type="checkbox">
                        <span class="checkbox-title">Разрешить отклики</span>
                    </label>
                </div>


                <div class="inline-edit-group">
                    <label class="inline-edit-status alignleft">
                        <span class="title">Статус</span>
                        <select name="_status">
                            <option value="publish">Опубликовано</option>
                            <option value="future">Запланировано</option>
                            <option value="pending">На утверждении</option>
                            <option value="draft">Черновик</option>
                        </select>
                    </label>



                    <label class="alignleft">
                        <input name="sticky" value="sticky" type="checkbox">
                        <span class="checkbox-title">Прилепить запись</span>
                    </label>



                </div>


            </div></fieldset>

        <p class="submit inline-edit-save">
            <a accesskey="c" href="#inline-edit" class="button-secondary cancel alignleft">Отмена</a>
            <input id="_inline_edit" name="_inline_edit" value="c19b9af89b" type="hidden">				<a accesskey="s" href="#inline-edit" class="button-primary save alignright">Обновить</a>
            <span class="spinner"></span>
            <input name="post_view" value="list" type="hidden">
            <input name="screen" value="edit-post" type="hidden">
            <span class="error" style="display:none"></span>
            <br class="clear">
        </p>
    </td></tr>

<tr id="bulk-edit" class="inline-edit-row inline-edit-row-post inline-edit-post bulk-edit-row bulk-edit-row-post bulk-edit-post" style="display: none"><td colspan="7" class="colspanchange">

        <fieldset class="inline-edit-col-left"><div class="inline-edit-col">
                <h4>Массовое редактирование</h4>
                <div id="bulk-title-div">
                    <div id="bulk-titles"></div>
                </div>




            </div></fieldset><fieldset class="inline-edit-col-center inline-edit-categories"><div class="inline-edit-col">


                <span class="title inline-edit-categories-label">Рубрики</span>
                <input name="post_category[]" value="0" type="hidden">
                <ul class="cat-checklist category-checklist">

                    <li id="category-1" class="popular-category"><label class="selectit"><input value="1" name="post_category[]" id="in-category-1" type="checkbox"> Без рубрики</label></li>
                </ul>


            </div></fieldset>


        <fieldset class="inline-edit-col-right"><label class="inline-edit-tags">
                <span class="title">Метки</span>
                <textarea cols="22" rows="1" name="tax_input[post_tag]" class="tax_input_post_tag"></textarea>
            </label><div class="inline-edit-col">

                <label class="inline-edit-author"><span class="title">Специальность по диплому</span><select name="post_author" class="authors">
                        <option value="-1">— Как есть —</option>
                        <option value="1">andrey</option>
                    </select></label>


                <div class="inline-edit-group">
                    <label class="alignleft">
                        <span class="title">Комментарии</span>
                        <select name="comment_status">
                            <option value="">— Как есть —</option>
                            <option value="open">Разрешить</option>
                            <option value="closed">Запретить</option>
                        </select>
                    </label>
                    <label class="alignright">
                        <span class="title">Уведомления</span>
                        <select name="ping_status">
                            <option value="">— Как есть —</option>
                            <option value="open">Разрешить</option>
                            <option value="closed">Запретить</option>
                        </select>
                    </label>
                </div>


                <div class="inline-edit-group">
                    <label class="inline-edit-status alignleft">
                        <span class="title">Статус</span>
                        <select name="_status">
                            <option value="-1">— Как есть —</option>
                            <option value="publish">Опубликовано</option>

                            <option value="private">Личное</option>
                            <option value="pending">На утверждении</option>
                            <option value="draft">Черновик</option>
                        </select>
                    </label>



                    <label class="alignright">
                        <span class="title">Прилеплена</span>
                        <select name="sticky">
                            <option value="-1">— Как есть —</option>
                            <option value="sticky">Прилеплена</option>
                            <option value="unsticky">Не прилеплена</option>
                        </select>
                    </label>



                </div>

                <label class="alignleft" for="post_format">
                    <span class="title">Формат</span>
                    <select name="post_format">
                        <option value="-1">— Как есть —</option>
                        <option value="0">Стандартный</option>
                        <option value="aside">Заметка</option>
                        <option value="image">Изображение</option>
                        <option value="link">Ссылка</option>
                        <option value="quote">Цитата</option>
                        <option value="status">Статус</option>
                    </select></label>

            </div></fieldset>

        <p class="submit inline-edit-save">
            <a accesskey="c" href="#inline-edit" class="button-secondary cancel alignleft">Отмена</a>
            <input name="bulk_edit" id="bulk_edit" class="button button-primary alignright" value="Обновить" accesskey="s" type="submit">			<input name="post_view" value="list" type="hidden">
            <input name="screen" value="edit-post" type="hidden">
            <span class="error" style="display:none"></span>
            <br class="clear">
        </p>
    </td></tr>
</tbody></table></form>

<div id="ajax-response"></div>
<br class="clear">
</div>


<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div><!-- wpbody -->
<div class="clear"></div>
<?php
}
