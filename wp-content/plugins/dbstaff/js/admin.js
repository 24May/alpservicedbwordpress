jQuery(document).ready(function() {
    jQuery.datepicker.setDefaults(
        jQuery.extend(jQuery.datepicker.regional["ru"])
    );
    jQuery('.datepicker').datepicker({
    dateFormat : "yy-mm-dd",
    monthNamesShort: [ "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря" ],
    dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
    dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    changeMonth: true,
    changeYear: true
    });
})
