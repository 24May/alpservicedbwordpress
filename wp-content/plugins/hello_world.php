<?php
/*
Plugin Name: Hello World
Plugin URI: http://i-novice.net/wpplugins/my_hello_world.zip
Author: Novice
Author URI: http://i-novice.net/
*/


$hello_world = "Hello, world!";



function hello_world($title) {
	global $hello_world;

	echo $title.' -> '.$hello_world;
}

add_filter('the_title', 'hello_world');

?>