<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wpdb');

/** Имя пользователя MySQL */
define('DB_USER', 'remoteuser');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '-RemoteUser2014-');

/** Имя сервера MySQL */
define('DB_HOST', '192.168.3.211');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-|;}`;&f~jHdON.}+R`S0*97TS^E?IW8=f^-mn+><H;B{eD!eI{fR{1:h`(`^=+u');
define('SECURE_AUTH_KEY',  'L=oRY2spF-vk=_c7I1_thFT-l#MPsfYvB`&iL` )c:;#Vp>hiQ.3C08M~dT|+HgT');
define('LOGGED_IN_KEY',    'cBm#V5&A=@|ywN:Ih5cu%A<i7TFu,/A^4L^e ZI|;Got4GNDpR{2EFda!p/>-(s4');
define('NONCE_KEY',        'OPZ5+8sA1TP>|@czFoVoRrnO(_<hZ>4amfvdGOzWqrnO+iT^lUkWLe=ad8$tu2)R');
define('AUTH_SALT',        'VlH:N0g*M{CrN^{=$gqoQYi^ +GS=QsRAf[iH.HE*AL(|9|M@W_=^i7MFRFPPXZ-');
define('SECURE_AUTH_SALT', '# |`3+pVM~CN!VnSGo5/S8XZQ v{m,|ehJA,y9Prpa 2Q*+?eF /5{`kB_eUC>-.');
define('LOGGED_IN_SALT',   'Y=mvyIljMG~KYtC8NAVl|1)7UwLuv@Lxz/jn.s3ispc?45fpMjxd!wDp4|P$7qwu');
define('NONCE_SALT',       ' WrXs4[d&Xp>b3Yln;2b&1:iAcq(M1h9BcW@=P-i)h=|VnB*r__MaVEi?1k6+(|.');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Язык локализации WordPress, по умолчанию английский.
 *
 * Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
 * для выбранного языка должен быть установлен в wp-content/languages. Например,
 * чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
 * и присвойте WPLANG значение 'ru_RU'.
 */
define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
